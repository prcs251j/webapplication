<?php
require_once 'rest_data.php';

//get the GET files from the string.  
if(isset($_GET['tablename']))
    {
        $tableName = $_GET['tablename'];
        $id = $_GET['id'];
        $idColumn = $_GET['idColumn'];
        $values = array($idColumn => $id);
        $result = delete($values, $tableName);
    }
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <p>
            <?php 
                if(isset($result))
                    {
                        echo 'Response code from API : '.$result;
                    }
            ?>
            
        </p>
    </body>
    
</html>
