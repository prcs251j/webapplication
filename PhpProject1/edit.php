<!--Commit 3-->
<?php
require_once 'rest_data.php';

$tableName = $_GET['tablename'];
$id = $_GET['id'];
$idColumn = $_GET['idColumn'];
$jsonData = array();
$tableString = '<table border="1"><tr>';
$inputString = '';

if(isset($tableName))
    {
        $results = getOne($tableName, $id);
        if($results)
        { 
            $jsonData = json_decode($results);
            //This foreach loop only wants to collect the column names,
            //so it only needs to look through the first row.
             
             //Now get the key of each of the entries that will represent the columns
            foreach($jsonData as $entry => $value)
            {
               $tableString .= '<th>'.$entry.'</th>';
               $inputString .= '<td><input type=\'text\' name=\''.$entry.'\' value=\''.$value.'\'/></td>';
            }
        }
    }
    
 $tableString .='</tr><tr>'.$inputString.'</tr></table>';

 if(isset($_POST['Submit']))
 {
     $values = array();
     $idValues = array($idColumn => $id);
     
     foreach($_POST as $key => $value)
     {
         if(!empty($value) && ($value != "Submit"))
         {
             $values[$key] = $value;
         }
     } 
     $result = update($idValues, $values, $tableName);
 }
    
    
?>
    <html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="<?php echo $_SERVER['PHP_SELF'].'?'.http_build_query($_GET); ?>" method="post">
            <h1>Edit Table</h1>
            <p>Please only enter data into the columns that require to be changed. Those left blank will
                not be updated.
            <p>
            <?php 
                echo $tableString;
                
                if(isset($_POST['Submit']))
                {
                    if(isset($result))
                    {
                        echo 'Response code from API : '.$result;
                    }
                }
            ?>
            </p>
            
         <input type="submit" value="Submit" name="Submit">
        </form>
        
<html>
    
    
</html>

