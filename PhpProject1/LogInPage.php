<!DOCTYPE html>
<?php
session_start();
require_once 'rest_data.php';
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
<style>


</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="#">TicketDirect</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li>
							<a href="index.php">Home</a>
						</li>
						<li>
							<a href="EventsPage.php">Theatre</a>
						</li>
						<li>
							<a href="EventsPage.php">Music</a>
						</li>
						<li>
							<a href="EventsPage.php">Festivals</a>
						</li>
						<li>
							<a href="EventsPage.php">Sports</a>
						</li>
						<li>
							<a href="EventsPage.php">Comedy</a>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div> 
						<button type="submit" class="btn btn-default">
							Search
						</button>
						<button type="submit" class="btn btn-default">
							Advanced Search
						</button>
					</form>
					              <?php
                                if(isset($_SESSION['CurrentUser'])){
                                	Echo "<ul class=\"nav navbar-nav navbar-right\">";
					Echo "<form class=\"navbar-form navbar-left\" role=\"search\">";
                                                
					Echo "<a href=\"ProfilePage.php\" class=\"button\">Your Profile |</a>";
                                        Echo "<a href=\"index.php?logout=1\" class=\"button\"> Sign Out</a>";
                                        
                                        if(isset($_GET['logout'])){
                                            session_unset();
                                            session_destroy();
                                            header("Refresh:0");
                                        }
                                                               
					Echo "</form>";
					Echo "</ul>";
                                       }
                                                ?>
				</div>

				
			</nav>
			
                
			    <div class="container" style="margin-top:40px">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong> Sign in to continue</strong>
					</div>
					<div class="panel-body">
						<form role="form" action="#" method="POST">
							<fieldset>
								<div class="row">
									<div class="center-block">
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-10  col-md-offset-1 ">
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-user"></i>
												</span> 
												<input class="form-control" placeholder="Username" name="loginname" type="text" autofocus>
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-lock"></i>
												</span>
												<input class="form-control" placeholder="Password" name="password" type="password" value="">
											</div>
										</div>
                                                                    
										<div class="form-group">
											<input type="submit" class="btn btn-lg btn-primary btn-block" name="signin" value="Sign in">
										</div>
                                                                          
									</div>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="panel-footer ">
						Don't have an account? <a href="RegisterPage.php" onClick=""> Sign Up Here </a>
					</div>
					<div class="panel-footer ">
						Forgot your password? <a href="RegisterPage.php" onClick=""> Reset Here </a>
					</div>
                </div>
			</div>
		</div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
<?php
if(isset($_POST['signin'])){
    
$empt = "[{\"PASSWORD\":0.0,\"USERNAME\":\"anon\"}";
$userName = $_POST['loginname'];
$passWord = $_POST['password'];
$results = getAll('CUSTOMERS');
$mergeAnon = $empt.$results;
$b = substr_replace($mergeAnon,',',35,1);
$data = json_decode($b, TRUE);

//print_r($data);

$UserKey = array_search($userName, array_column($data, 'USERNAME'));
$pass = array_column($data, 'PASSWORD');

$d = $pass[$UserKey];

if ($d == $passWord && $UserKey != 0 && $userName != '' && $passWord != '') {
     $_SESSION['CurrentUser'] = $userName;
        echo "<script type=\"text/javascript\">";
        echo "window.location.href = \"ProfilePage.php\"";
        echo "</script>";

}else{
    
    $message = "You have entered a incorrect Password/Username";
    echo "<script type='text/javascript'>alert('$message');</script>";
}


}
?>
    
</body>
</html>