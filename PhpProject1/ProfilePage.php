<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
<style>


</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="#">TicketDirect</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li>
							<a href="index.php">Home</a>
						</li>
						<li>
							<a href="EventsPage.php">Theatre</a>
						</li>
						<li>
							<a href="EventsPage.php">Music</a>
						</li>
						<li>
							<a href="EventsPage.php">Festivals</a>
						</li>
						<li>
							<a href="EventsPage.php">Sports</a>
						</li>
						<li>
							<a href="EventsPage.php">Comedy</a>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div> 
						<button type="submit" class="btn btn-default">
							Search
						</button>
						<button type="submit" class="btn btn-default">
							Advanced Search
						</button>
					</form>
				              <?php
                                if(isset($_SESSION['CurrentUser'])){
                                	Echo "<ul class=\"nav navbar-nav navbar-right\">";
					Echo "<form class=\"navbar-form navbar-left\" role=\"search\">";
                                                
					Echo "<a href=\"ProfilePage.php\" class=\"button\">Your Profile |</a>";
                                        Echo "<a href=\"index.php?logout=1\" class=\"button\"> Sign Out</a>";
                                        
                                        if(isset($_GET['logout'])){
                                            session_unset();
                                            session_destroy();
                                            header("Refresh:0");
                                        }
                                                               
					Echo "</form>";
					Echo "</ul>";
                                       }else{
					Echo "<ul class=\"nav navbar-nav navbar-right\">";
					Echo "<form class=\"navbar-form navbar-left\" role=\"search\">";
                                                
					Echo "<a href=\"LogInPage.php\" class=\"button\">Sign In |</a>";
                                        Echo "<a href=\"RegisterPage.php\" class=\"button\"> Register</a>";
                                                               
					Echo "</form>";
					Echo "</ul>";
                                        }
                                                ?>
				</div>

				
			</nav>
			
                    <?php
                    echo "<center>";
                    echo "<font size=\"16\">";
                    echo "Welcome ". $_SESSION['CurrentUser'] . " to your Profile Page.";
                    echo "</center>";
                    echo "</font>";
                    
                    ?>
			
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>