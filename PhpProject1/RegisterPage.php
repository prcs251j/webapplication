<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
<style>


</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      
      <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="#">TicketDirect</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li>
							<a href="index.php">Home</a> 
						</li>
						<li>
							<a href="EventsPage.php">Theatre</a>
						</li>
						<li>
							<a href="EventsPage.php">Music</a>
						</li>
						<li>
							<a href="EventsPage.php">Festivals</a>
						</li>
						<li>
							<a href="EventsPage.php">Sports</a>
						</li>
						<li>
							<a href="EventsPage.php">Comedy</a>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div> 
						<button type="submit" class="btn btn-default">
							Search
						</button>
						<button type="submit" class="btn btn-default">
							Advanced Search
						</button>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<form class="navbar-form navbar-left" role="search">
					</form>
					</ul>
				</div>	
			</nav>
                     

			
	<div class="container">
    <h1 class="well">Registration Form</h1> 
	<div class="col-lg-12 well">
	<div class="row">
				  <form role="form" action="#" method="POST" onsubmit="return myFunction()">
					<div class="col-sm-12">
						<div class="row"> 
							<div class="col-sm-6 form-group">
								<label>First Name</label>
                                                                <input  id="fname"  type="text" placeholder="Enter First Name Here.." name="firstname" required class="form-control" value="<?php if(isset($_POST['firstname'])){ echo $_POST['firstname']; }?>" />
							</div>
							<div class="col-sm-6 form-group">
								<label>Last Name</label>
								<input  id="lname"  type="text" placeholder="Enter Last Name Here.." name="lastname" required class="form-control" value="<?php if(isset($_POST['lastname'])){ echo $_POST['lastname']; }?>" />
							</div>
						</div>			
                        <div class="row">						
						<div class="col-sm-6 form-group">
							<label>Address 1</label>
							<input type="text" placeholder="Enter Address Here.." name="address1" required class="form-control" value="<?php if(isset($_POST['address1'])){ echo $_POST['address1']; }?>" />
						</div>	
							<div class="col-sm-6 form-group">
							<label>Address 2</label>
							<input type="text" placeholder="Enter Address Here.." name="address2" class="form-control" value="<?php if(isset($_POST['address2'])){ echo $_POST['address2']; }?>" />
						</div>
                        </div>						
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>City</label>
								<input type="text" placeholder="Enter City Name Here.." name="city" required class="form-control" value="<?php if(isset($_POST['city'])){ echo $_POST['city']; }?>" />
							</div>	
							<div class="col-sm-6 form-group">
								<label>Post Code</label>
								<input  id="pcode"   type="text" placeholder="Enter Post Code Here.." name="postcode" required class="form-control" value="<?php if(isset($_POST['postcode'])){ echo $_POST['postcode']; }?>" />
							</div>		
						</div>
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>Secret Question</label>
								<input type="text" placeholder="Enter Secret Question here.." name="question" required class="form-control" value="<?php if(isset($_POST['question'])){ echo $_POST['question']; }?>" />
							</div>		
							<div class="col-sm-6 form-group">
								<label>Secret Answer</label>
								<input type="text" placeholder="Enter Secret Answer here.." name="answer" required class="form-control" value="<?php if(isset($_POST['answer'])){ echo $_POST['answer']; }?>" />
							</div>	
						</div>						
					<div class="form-group">
						<label>Mobile Number</label>
						<input id="mnumb" type="text" placeholder="Enter Mobile Number Here.." name="mobilenumber" required class="form-control" value="<?php if(isset($_POST['mobilenumber'])){ echo $_POST['mobilenumber']; }?>" />
					</div>		
					<div class="form-group">
						<label>Email Address</label>
						<input id="edress" type="text" placeholder="Enter Email Address Here.." name="email" required class="form-control" value="<?php if(isset($_POST['email'])){ echo $_POST['email']; }?>" />
					</div>	
                                            <div class="row">
                                            <div class="col-sm-12 form-group">
                                                <label>Date of Birth: </label>
                                            </div>
                                            </div>
						<div class="row">
							<div class="col-sm-2 form-group">
								<label>Day</label>
								<input id="dateday"  type="text" placeholder="Eg. 05" name="day" required class="form-control" value="<?php if(isset($_POST['day'])){ echo $_POST['day']; }?>" />
							</div>	
                                                        <div class="col-sm-2 form-group">
								<label>Month</label>
								<input id="datemonth"  type="text" placeholder="Eg. 11" name="month" required class="form-control" value="<?php if(isset($_POST['month'])){ echo $_POST['month']; }?>" />
							</div>	
                                                    	<div class="col-sm-2 form-group">
								<label>Year</label>
								<input id="dateyear"  type="text" placeholder="Eg. 1981" name="year" required class="form-control" value="<?php if(isset($_POST['year'])){ echo $_POST['year']; }?>" />
							</div>	
							<div class="col-sm-6 form-group">
							</div>	
						</div>				
												<div class="row">
							<div class="col-sm-6 form-group">
								<label>Username</label>
								<input id="uname" type="text" placeholder="Enter Username here.." name="username" required class="form-control" value="<?php if(isset($_POST['username'])){ echo $_POST['username']; }?>" />
							</div>		
							<div class="col-sm-6 form-group">
                                                            <label>Preferences (Optional)</label>
							    <input id="pref" type="text" placeholder="Enter Preferences. For example: Music, Theatre" name="genres" class="form-control" value="<?php if(isset($_POST['genres'])){ echo $_POST['genres']; }?>" />
							</div>	
						</div>			
						<div class="row"> 
							<div class="col-sm-6 form-group">
								<label>Password</label>
                                                                <input id="pass1" type=password placeholder="Enter Password here.." required name="password1" class="form-control" value="<?php if(isset($_POST['password1'])){ echo $_POST['password1']; }?>" />
							</div>		
							<div class="col-sm-6 form-group">
								<label>Type Password again</label>
                                                                <input id="pass2" type=password placeholder="Enter Password again here.." name="password2" class="form-control" required value="<?php if(isset($_POST['password2'])){ echo $_POST['password2']; }?>" />
							</div>	
						</div>
                                         
					<div class="form-group">
				        <input type="submit" class="btn btn-lg btn-primary btn-block" name="submit" value="Submit">
				        </div>				
					</div>
				</form> 
				</div>
	</div>
	</div>
                    


                    <?php
                    if(isset($_POST['submit'])){
                        
                        
                        
                    require_once 'rest_data.php';
                    
                    $passWord1 = $_POST['password1'];
                    $passWord2 = $_POST['password2'];
                    $username = $_POST['username'];
                    $genres = $_POST['genres'];
                    $day = $_POST['day'];
                    $month = $_POST['month'];
                    $year = $_POST['year'];
                    $email = $_POST['email'];
                    $mobilenumber = $_POST['mobilenumber'];
                    $question = $_POST['question'];
                    $answer = $_POST['answer'];
                    $city = $_POST['city'];
                    $postcode = $_POST['postcode'];
                    $address1 = $_POST['address1'];
                    $address2 = $_POST['address2'];
                    $firstname = $_POST['firstname'];
                    $lastname = $_POST['lastname'];
                    
                  //  	1979-03-08T00:00:00
                    $dob = $year.'-'.$month.'-'.$day.'T00:00:00';
                //   $dob = '1970-09-05T00:00:00';
                  
             //     $test = $passWord1.$passWord2.$username.$genres.$dob.$email.$mobilenumber.$question.$answer.$city.$postcode.$address1.$address2.$firstname.$lastname;
                    
                  
                  
                  $tableName = 'CUSTOMERS';
                  
                  $values = Array ( "COSTOMER_ID" => 341, "FORENAME" => $firstname, "SURNAME" => $lastname, "DATE_OF_BIRTH" => $dob, "EMAIL_ADDRESS" => $email, "MOBILE_NUMBER" => $mobilenumber, "USERNAME" => $username, "PASSWORD" => $passWord1, "SECRET_QUESTION" => $question, "SECRET_ANSWER" => $answer, "PREFRENCES" => $genres, "CITY" => $city, "ADDRESS1" => $address1, "POST_CODE" => $postcode);
                  
        $emptt = "[{\"PASSWORD\":0.0,\"USERNAME\":\"moijyfbga01255447689\"}";       
        $Resultss = getAll('CUSTOMERS');
        $mergeAnonn = $emptt.$Resultss;
        $f = substr_replace($mergeAnonn,',',51,1);
        $datar = json_decode($f, TRUE);
        $passs = array_column($datar, 'USERNAME');
        $Key = array_search($username, $passs);
                 
        if($Key == 0){
   //   $exists = "Username is available";
//echo "<script type='text/javascript'>alert('$exists');</script>";
      $http = insert($values, $tableName); 
            
      if($http < 300){
      echo '<script type="text/javascript">'; 
      echo 'alert("You have succsesfully registered");'; 
      echo 'window.location.href = "LogInPage.php";';
      echo '</script>';
      }else{
          
         $fail = "Server side error. Cannot take your registration at this time";
echo "<script type='text/javascript'>alert('$fail');</script>";  
                
                
            }
            
            
        }else{
            
$fail2 = "Username already exists";
echo "<script type='text/javascript'>alert('$fail2');</script>";  
echo "<script>";     
echo "var uname = document.getElementById(\"uname\").value;";
echo "document.getElementById(\"uname\").style.borderColor = \"#E34234\";";
echo "</script>";
            
        }
                    }
                    
                    ?>
                    
    
<script>
function myFunction() {
    var pass1 = document.getElementById("pass1").value;
    var pass2 = document.getElementById("pass2").value;
    var fname = document.getElementById("fname").value;
    var lname = document.getElementById("lname").value;
    var pcode = document.getElementById("pcode").value;
    var mnumb = document.getElementById("mnumb").value;
    var edress = document.getElementById("edress").value;
    var dateday = document.getElementById("dateday").value;
    var datemonth = document.getElementById("datemonth").value;
    var dateyear = document.getElementById("dateyear").value;
    var pref = document.getElementById("pref").value;
    var uname = document.getElementById("uname").value;
   // var uname = document.getElementById("uname").value;
    //var current = Date().getFullYear();
    var ok = true;
    
    reg = /^[a-zA-Z]+$/;
    if(!reg.test(fname)) {
        document.getElementById("fname").style.borderColor = "#E34234";
        alert("First name must only contain letters");
        ok = false;
    }
    
    reg2 = /^[a-zA-Z]+$/;
    if(!reg2.test(lname)) {
        document.getElementById("lname").style.borderColor = "#E34234";
        alert("Last name must only contain letters");
        ok = false;
    }
    
    reg3 =  /^(GIR 0AA)|((([A-Z][0-9]{1,2})|(([A-Z][A-HJ-Y][0-9]{1,2})|(([A-Z][0-9][A-Z])|([A-Z][A-HJ-Y][0-9]?[A-Z])))) [0-9][A-Z]{2})$/;
    if(!reg3.test(pcode)) {
        document.getElementById("pcode").style.borderColor = "#E34234";
        alert("Post code is not correct. Make sure you include a SPACE.");
        ok = false;
    }
    
    reg4 = /^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/;
    if(!reg4.test(mnumb)) {
        document.getElementById("mnumb").style.borderColor = "#E34234";
        alert("Incorrect UK mobile number entered");
        ok = false;
    }
    
    reg5 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!reg5.test(edress)) {
        document.getElementById("edress").style.borderColor = "#E34234";
        alert("Incorrect email format entered");
        ok = false;
    }
    
    reg6 = /^(([0][1-9])|([1-2][0-9])|(3[01]))$/;
    if(!reg6.test(dateday)) {
        document.getElementById("dateday").style.borderColor = "#E34234";
        alert("You must enter the day of your birth. Eg: '05' (Represents the 5th Day). Do not enter a number greater than 31");
        ok = false;
    }
    
    reg7 = /(1[0-2]|0[1-9])/;
    if(!reg7.test(datemonth)) {
        document.getElementById("datemonth").style.borderColor = "#E34234";
        alert("You must enter the month of birth. Eg: '12' (Represents December). Do not enter a number greater than 12");
        ok = false;
    }
    
    reg8 = /^\d{4}$/;
    if(!reg8.test(dateyear)) {
        document.getElementById("dateyear").style.borderColor = "#E34234";
        alert("You must enter the year of birth. Eg: '1981' (Represents Year). Do not enter a year greater than current");
        ok = false;
    }    
    
    reg10 = /^[A-Za-z0-9\w\s]+(?:,[A-Za-z0-9\w\s]+)*$/;
    if(!reg10.test(pref) && pref !== "") {
        document.getElementById("pref").style.borderColor = "#E34234";
        alert("You must list your prefernces followed by a comma");
        ok = false;
    } 
    
    reg11 = /^([a-zA-Z0-9_-]){3,10}$/;
    if(!reg11.test(uname)) {
        document.getElementById("uname").style.borderColor = "#E34234";
        alert("Username can only be 3 to 10 characters long");
        ok = false;
    } 
    
    //reg9 = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    reg9 = /^[0-9]{5,8}$/;
    if(!reg9.test(pass1)) {
        document.getElementById("pass1").style.borderColor = "#E34234";
       // alert("Your password must contain a minimum of 8 characters. This must include 1 digit, 1 lower case letter and 1 upper case letter");
        alert("Your password can only contain numbers. The number must be a length of 5 to 8");
        ok = false;
    } 
    
    if (pass1 != pass2) {
        document.getElementById("pass1").style.borderColor = "#E34234";
        document.getElementById("pass2").style.borderColor = "#E34234";
        alert("Passwords do not match");
        ok = false;
    }

    return ok;
}
</script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>