<!--comment for testing-->
<!DOCTYPE html>
<?php
session_start();
?>
<?php
    ini_set('display_errors', 1);
    require_once 'rest_data.php';
    
    if(isset($_POST['table']))
    {
        $tableName = $_POST['table'];
    }
    if(isset($_POST['insert']))
    {
        $values = array();
        $tableName = $_POST['tablename'];
        
        foreach($_POST as $key => $value)
        {
            if(!empty($value) && ($value != "Submit") && ($key != "insert") && ($key != "table") && ($key != "tablename"))
            {
                $values[$key] = $value;
            }
     } 
    // echo $values;
    // print_r($values);
     $httpCode = insert($values, $tableName);    
    }

?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
<style>


</style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="#">TicketDirect</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li>
							<a href="index.php">Home</a>
						</li>
						<li>
							<a href="#">Theatre</a>
						</li>
						<li>
							<a href="#">Music</a>
						</li>
						<li>
							<a href="#">Festivals</a>
						</li>
						<li>
							<a href="#">Sports</a>
						</li>
						<li>
							<a href="#">Comedy</a>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div> 
						<button type="submit" class="btn btn-default">
							Search
						</button>
						<button type="submit" class="btn btn-default">
							Advanced Search
						</button>
					</form>
              <?php
                                if(isset($_SESSION['CurrentUser'])){
                                	Echo "<ul class=\"nav navbar-nav navbar-right\">";
					Echo "<form class=\"navbar-form navbar-left\" role=\"search\">";
                                                
					Echo "<a href=\"ProfilePage.php\" class=\"button\">Your Profile |</a>";
                                        Echo "<a href=\"index.php?logout=1\" class=\"button\"> Sign Out</a>";
                                        
                                        if(isset($_GET['logout'])){
                                            session_unset();
                                            session_destroy();
                                            header("Refresh:0");
                                        }
                                                               
					Echo "</form>";
					Echo "</ul>";
                                       }else{
					Echo "<ul class=\"nav navbar-nav navbar-right\">";
					Echo "<form class=\"navbar-form navbar-left\" role=\"search\">";
                                                
					Echo "<a href=\"LogInPage.php\" class=\"button\">Sign In |</a>";
                                        Echo "<a href=\"RegisterPage.php\" class=\"button\"> Register</a>";
                                                               
					Echo "</form>";
					Echo "</ul>";
                                        }
                                                ?>
				</div>	
			</nav>
			
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <h1>Select Table</h1>
            
            <select name="table" id="ddTable">
            <option value="EVENTS">Events</option>
            <option value="BOOKING_STATUS">Booking Status</option>
            <option value="BOOKINGS">Bookings</option>
            <option value="CUSTOMERS">Customers</option>
            <option value="GENRES">Genres</option>
            <option value="SHOWS">Shows</option>
            <option value="TYPES">Types</option>
            <option value="VENUES">Venues</option>
            <option value="WATCHLIST">Watch List</option>
            <option value="CITIES">Cities</option>
        </select>
         <input type="submit" value="Submit">
       
        <p>
            
            <?php
                if(isset($httpCode))
                {
                    echo "Response from API : ".$httpCode;
                }
                //NOTE: This relies on ALL the results coming back as an array.
  
                if(isset($tableName))
                {
                    $results = getAll($tableName);
                    
                
                    //Check to see there is a value that has come back!
                    if($results)
                    {
                
                        $json = json_decode($results);
                        
                        //The assumption here is that the first column in the results is the key
                        //Find the ID column.  Assume the first column of the returned row
                        $idColumn = key($json[0]);
                        //declare the variable for use in the foreach loop
                        $columns = array();
                        //This foreach loop only wants to collect the column names,
                        //so it only needs to look through the first row.
                        $row = $json[0];
                        //Now get the key of each of the entries that will represent the columns
                        
                        foreach($row as $entry => $values)
                        {
                            $columns[] = $entry;
                        }
                        
                        
                        //time to build up the table.
                        $tableString = '<table border="1"><tr>';
                        $inputString = '';
                        $insertString = '';
                        //Put in all the column headings.
                        foreach($columns as $column)
                        {
                               $tableString .= '<th>'.$column.'</th>';
                               $inputString .= '<th>'.$column.'</th>';
                               $insertString .= '<td><input type=\'text\' name=\''.$column.'\'/></td>';
                   
                        }
                        //Remember the edit and delete links.
                        $tableString .= '<th>Edit</th><th>Delete</th></tr>';
                      
                        echo $tableString;
                        //Time now to navigate through all the $json array and output all
                        //the values.
                        foreach($json as $row)
                        {   
 
                            echo '<tr>'; 
                           foreach($row as $cell => $value)
                           {
                               echo '<td>'.$value.'</td>';
                           }
                           
                            
                           
                           echo '<td><a href=edit.php?tablename='.$tableName.'&id='.rawurlencode($row->$idColumn).'&idColumn='.$idColumn.'>Edit</a></td><td><a href=delete.php?tablename='.$tableName.'&id='.rawurlencode($row->$idColumn).'&idColumn='.$idColumn.'>delete</a></td></tr>';
                        }
                        echo '<tr>'.$inputString.'<th colspan=2>New</th></tr><tr>'.$insertString.'<td colspan=2><input type=\'Submit\' name=\'insert\' value=\'Insert\'></td></tr>';
                        
                        echo '</table>';
                        echo '<input type=\'hidden\' name=\'tablename\' value=\''.$tableName.'\'>';
                }
          }
 ?>
        </p>
       
        </form>

	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>