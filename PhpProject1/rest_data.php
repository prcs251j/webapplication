<?php



function getAll($resourceName){
  
    $result = file_get_contents('http://xserve.uopnet.plymouth.ac.uk/modules/isad251/mmetcalfe/api/'.$resourceName); 

    return $result;
 
}

function getOne($resourceName, $id){
    
   $result2 = file_get_contents('http://xserve.uopnet.plymouth.ac.uk/modules/isad251/mmetcalfe/api/'.$resourceName.'/'.rawurlencode($id)); 
  
     return $result2;
    
}

function update(array $id, array $values, $resourceName){
    $a = json_encode($values);
    $b = $id[key($id)];
    $result2 = 'http://xserve.uopnet.plymouth.ac.uk/modules/isad251/mmetcalfe/api/'.$resourceName.'/'.rawurlencode($b);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $result2);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); //
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $a);
    curl_exec($ch);
    return curl_getinfo($ch, CURLINFO_HTTP_CODE);
    
}

function insert(array $values, $resourceName){
    $a = json_encode($values);
    $result = 'http://xserve.uopnet.plymouth.ac.uk/modules/isad251/mmetcalfe/api/'.$resourceName;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $result);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $a);
    curl_exec($ch);
    return curl_getinfo($ch, CURLINFO_HTTP_CODE);
}

function delete(array $id, $resourceName){
    $a = $id[key($id)];
    $result2 = 'http://xserve.uopnet.plymouth.ac.uk/modules/isad251/mmetcalfe/api/'.$resourceName.'/'.rawurlencode($a);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $result2);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_exec($ch);
    return curl_getinfo($ch, CURLINFO_HTTP_CODE);
}